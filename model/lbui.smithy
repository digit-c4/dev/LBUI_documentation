$version: "2"
namespace experimental.lbui

use aws.protocols#restJson1

/// API managing loadbalancing assets.
@httpBasicAuth
@restJson1
service lbui {
    version: "2023-12-06"
    operations: [InstallNativeVirtualServer, CheckVipCompliance, CreateVirtualServerWithDeps, DeleteVirtualServerWithDeps,
                CreateVirtualServer, DeleteVirtualServer, GetVirtualServerStatus, GetVirtualServerTimeout, CreateMonitor, DeleteMonitor,
                CreatePool, CreateSingleNode, DeleteSingleNode, CreateVirtualServerNodes, DeleteVirtualServerNodes]
}

/// Routes (manually set)
@http(method: "POST", uri: "/virtual-server/do-all2/{uid}", code: 200)
operation InstallNativeVirtualServer {
    input: InstallNativeVirtualServerInput
    output: InstallNativeVirtualServerOutput
    errors: [UnauthorizedError]
}

@http(method: "POST", uri: "/virtual-server/check-all2/{uid}", code: 200)
operation CheckVipCompliance {
    input: CheckVipComplianceInput
    output: CheckVipComplianceOutput
    errors: [UnauthorizedError]
}
@http(method: "POST", uri: "/virtual-server/do-all/{uid}", code: 200)
operation CreateVirtualServerWithDeps {
    input: CreateVirtualServerWithDepsInput
    output: CreateVirtualServerWithDepsOutput
    errors: [UnauthorizedError]
}
@http(method: "POST", uri: "/virtual-server/delete-all/{uid}", code: 200)
operation DeleteVirtualServerWithDeps {
    input: DeleteVirtualServerWithDepsInput
    output: DeleteVirtualServerWithDepsOutput
    errors: [UnauthorizedError]
}
@http(method: "POST", uri: "/virtual-server/create/{uid}", code: 200)
operation CreateVirtualServer {
    input: CreateVirtualServerInput
    output: CreateVirtualServerOutput
    errors: [UnauthorizedError]
}
@http(method: "POST", uri: "/virtual-server/delete/{uid}", code: 200)
operation DeleteVirtualServer {
    input: DeleteVirtualServerInput
    output: DeleteVirtualServerOutput
    errors: [UnauthorizedError]
}


@readonly
@http(method: "GET", uri: "/virtual-server/get-status/{uid}", code: 200)
operation GetVirtualServerStatus {
    input: GetVirtualServerStatusInput
    output: GetVirtualServerStatusOutput
    errors: [UnauthorizedError]
}
@readonly
@http(method: "GET", uri: "/virtual-server/get-timeout/{uid}", code: 200)
operation GetVirtualServerTimeout {
    input: GetVirtualServerTimeoutInput
    output: GetVirtualServerTimeoutOutput
    errors: [UnauthorizedError]
}

@http(method: "POST", uri: "/monitor/create/{uid}", code: 200)
operation CreateMonitor {
    input: CreateMonitorInput
    output: CreateMonitorOutput
    errors: [UnauthorizedError]
}
@http(method: "POST", uri: "/monitor/delete/{uid}", code: 200)
operation DeleteMonitor {
    input: DeleteMonitorInput
    output: DeleteMonitorOutput
    errors: [UnauthorizedError]
}

@http(method: "POST", uri: "/pool/create/{uid}", code: 200)
operation CreatePool {
    input: CreatePoolInput
    output: CreatePoolOutput
    errors: [UnauthorizedError]
}

@http(method: "POST", uri: "/node-single/create/{node_id}/{env}", code: 200)
operation CreateSingleNode {
    input: CreateSingleNodeInput
    output: CreateSingleNodeOutput
    errors: [UnauthorizedError]
}

@http(method: "POST", uri: "/node-single/delete/{node_id}/{env}", code: 200)
operation DeleteSingleNode {
    input: DeleteSingleNodeInput
    output: DeleteSingleNodeOutput
    errors: [UnauthorizedError]
}
@http(method: "POST", uri: "/nodes-vs/create/{vs_id}", code: 200)
operation CreateVirtualServerNodes {
    input: CreateVirtualServerNodesInput
    output: CreateVirtualServerNodesOutput
    errors: [UnauthorizedError]
}
@http(method: "POST", uri: "/nodes-vs/delete/{vs_id}", code: 200)
operation DeleteVirtualServerNodes {
    input: DeleteVirtualServerNodesInput
    output: DeleteVirtualServerNodesOutput
    errors: [UnauthorizedError]
}

@readonly
@http(method: "GET", uri: "/", code: 200)
operation GetDefaultRoute {
    output: GetDefaultRouteOutput
    errors: [UnauthorizedError]
}

/// Dedicated structures
@error("client")
@httpError(401)
structure UnauthorizedError {}

@input
structure InstallNativeVirtualServerInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure InstallNativeVirtualServerOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}



@input
structure CheckVipComplianceInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure CheckVipComplianceOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure CreateVirtualServerWithDepsInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure CreateVirtualServerWithDepsOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure DeleteVirtualServerWithDepsInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure DeleteVirtualServerWithDepsOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure CreateVirtualServerInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure CreateVirtualServerOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure DeleteVirtualServerInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure DeleteVirtualServerOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}


@input
structure GetVirtualServerStatusInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure GetVirtualServerStatusOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure GetVirtualServerTimeoutInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure GetVirtualServerTimeoutOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}

@input
structure CreateMonitorInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure CreateMonitorOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure DeleteMonitorInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure DeleteMonitorOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}

@input
structure CreatePoolInput {
    //
    @required
    @httpLabel
    uid: String,
}
@output
structure CreatePoolOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}

@input
structure CreateSingleNodeInput {
    //
    @required
    @httpLabel
    node_id: String,

    //
    @required
    @httpLabel
    env: String,
}
@output
structure CreateSingleNodeOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure DeleteSingleNodeInput {
    //
    @required
    @httpLabel
    node_id: String,

    //
    @required
    @httpLabel
    env: String,
}
@output
structure DeleteSingleNodeOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure CreateVirtualServerNodesInput {
    //
    @required
    @httpLabel
    vs_id: String,
}
@output
structure CreateVirtualServerNodesOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}
@input
structure DeleteVirtualServerNodesInput {
    //
    @required
    @httpLabel
    vs_id: String,
}
@output
structure DeleteVirtualServerNodesOutput {
    //
    @required
    errors: ListError,

    @required
    resultscount: Integer,

    @required
    status: String,

    @required
    warnings: ListWarnings,
}

@output
structure GetDefaultRouteOutput {
    // Default, static String return value.
}



list ListError {
    member: Error
}

list ListWarnings {
    member: Warning
}


/// Common structures
structure Error {
    @required
    error: String
}
structure Warning {
    @required
    warning: String
}
