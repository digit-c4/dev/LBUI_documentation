FROM gradle:8.5.0-jdk11
WORKDIR /smithy
COPY . /smithy
COPY ./gradle.properties /.gradle
RUN /smithy/smithy-install/smithy/install && rm -rf /smithy/smithy-install
RUN gradle build --no-daemon

ENTRYPOINT ["tail"]
CMD ["-f","/dev/null"]